package ru.vkandyba.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.service.IProjectService;
import ru.vkandyba.tm.api.service.IProjectTaskService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(@NotNull ITaskRepository taskRepository,@NotNull IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task bindTaskToProjectById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new RuntimeException();
        if (!taskRepository.existsById(taskId)) throw new RuntimeException();
        return taskRepository.bindTaskToProjectById(userId, projectId, taskId);
    }

    @Nullable
    @Override
    public Task unbindTaskToProjectById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        if (!taskRepository.existsById(taskId)) return null;
        return taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
    }

    @Override
    public void removeAllTaskByProjectId(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeAllTaskByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Project removeById(@Nullable String userId, @Nullable String projectId) {
        if(userId == null || userId.isEmpty()) throw new EmptyIdException();
        removeAllTaskByProjectId(userId, projectId);
        return projectRepository.removeById(projectId);
    }

}
