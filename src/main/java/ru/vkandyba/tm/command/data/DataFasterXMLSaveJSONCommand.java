package ru.vkandyba.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.Domain;

import java.io.FileOutputStream;

public class DataFasterXMLSaveJSONCommand extends AbstractDataCommand{

    @Override
    public String name() {
        return "data-fasterxml-save-json";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Data JSON save ...";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON_FASTERXML);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
