package ru.vkandyba.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum Status {

    NON_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Complete");

    @NotNull
    private String displayName;

    Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

}
