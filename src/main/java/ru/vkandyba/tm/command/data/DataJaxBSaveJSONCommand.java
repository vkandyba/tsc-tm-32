package ru.vkandyba.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DataJaxBSaveJSONCommand extends AbstractDataCommand{

    @Override
    public String name() {
        return "data-jaxb-save-json";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Data JaxB save JSON";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.setProperty(JAVAX_XML_BIND_CONTEXT_FACTORY, ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY);
        @NotNull Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(ECLIPSELINK_MEDIA_TYPE, APPLICATION_JSON);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_JSON_JAXB);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();

    }
}
