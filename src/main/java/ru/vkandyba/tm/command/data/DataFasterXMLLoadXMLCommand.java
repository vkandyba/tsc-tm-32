package ru.vkandyba.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataFasterXMLLoadXMLCommand extends AbstractDataCommand{

    @NotNull
    @Override
    public String name() {
        return "data-fasterxml-load-xml";
    }

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Data FasterXML load XML...";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_XML_FASTERXML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }
}
