package ru.vkandyba.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.vkandyba.tm.enumerated.Role;

import java.util.UUID;

@Setter
@Getter
public class User extends AbstractEntity {

    private String login;

    private String passwordHash;

    private String email;

    private String firstName;

    private String lastName;

    private String middleName;

    private Role role = Role.USER;

    private Boolean locked = false;

}
