package ru.vkandyba.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IRepository;
import ru.vkandyba.tm.api.service.IService;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.model.AbstractEntity;
import ru.vkandyba.tm.repository.AbstractRepository;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected IRepository<E> repository;

    public AbstractService(@NotNull IRepository<E> repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public Boolean existsByIndex(@Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.existsByIndex(index);
    }

    @NotNull
    @Override
    public Boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public E findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    public E findByIndex(@Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(index);
    }

    @NotNull
    @Override
    public E removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @NotNull
    @Override
    public E removeByIndex(@Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.removeByIndex(index);
    }

    @NotNull
    @Override
    public E add(@Nullable E entity) {
        if (entity == null) throw new RuntimeException();
        return repository.add(entity);
    }

    @Override
    public void remove(@Nullable E entity) {
        if (entity == null) throw new RuntimeException();
        repository.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable Comparator<E> comparator) {
        if(comparator == null) throw new RuntimeException();
        return repository.findAll(comparator);
    }

    @Override
    public void addAll(@Nullable List<E> entities) {
        if(entities == null) throw new RuntimeException();
        repository.addAll(entities);
    }

    @Override
    public void clear() {
        repository.clear();
    }

}
