package ru.vkandyba.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.vkandyba.tm.api.repository.ICommandRepository;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.api.service.*;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.command.project.*;
import ru.vkandyba.tm.command.system.*;
import ru.vkandyba.tm.command.task.*;
import ru.vkandyba.tm.command.user.*;
import ru.vkandyba.tm.constant.TerminalConst;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.system.UnknownCommandException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.repository.*;
import ru.vkandyba.tm.service.*;
import ru.vkandyba.tm.util.SystemUtil;
import ru.vkandyba.tm.util.TerminalUtil;

import javax.management.ReflectionException;
import java.io.File;
import java.lang.reflect.Member;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Set;
import java.util.jar.Manifest;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
    @NotNull
    private final ILogService logService = new LogService();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);
    @NotNull
    private final Backup backup = new Backup(this);
    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @SneakyThrows
    private void initCommands(){
            @NotNull final Reflections reflections = new Reflections("ru.vkandyba.tm.command");
            @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(ru.vkandyba.tm.command.AbstractCommand.class);
            for(Class<? extends AbstractCommand> clazz: classes){
                if(Modifier.isAbstract(clazz.getModifiers())) continue;
                registry(clazz.newInstance());
            }
    }

    private void initData() {
        projectService.add(new Project("Project Q", "-")).setStatus(Status.COMPLETED);
        projectService.add(new Project("Project B", "-"));
        projectService.add(new Project("Project F", "-")).setStatus(Status.IN_PROGRESS);
        projectService.add(new Project("Project D", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task A", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task D", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task V", "-")).setStatus(Status.IN_PROGRESS);
        taskService.add(new Task("Task S", "-"));
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    private void initPID(){
        @NotNull final String fileName = "tsc-tm.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes() );
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        logService.debug("Test environment.");
        @Nullable String command = "";
        initCommands();
        initData();
        initUsers();
        initPID();
        backup.init();
        fileScanner.init();
        @NotNull final Scanner scanner = new Scanner(System.in);
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("ENTER COMMAND");
                command = scanner.nextLine();
                logService.command(command);
                execute(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
        backup.stop();
    }


    private void registry(@Nullable AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void execute(@Nullable String command) {
        if (command == null) return;
        final AbstractCommand abstractCommand = getCommandService().getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException();
        final Role[] roles = abstractCommand.roles();
        authService.checkRoles(roles);
        abstractCommand.execute();
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

}
