package ru.vkandyba.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.FileOutputStream;

public class DataJaxBSaveXMLCommand extends AbstractDataCommand{

    @Override
    public String name() {
        return "data-jaxb-save-xml";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Data JaxB save XML";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_XML_JAXB);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }
}
