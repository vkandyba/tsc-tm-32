package ru.vkandyba.tm.command.data;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataFasterXMLLoadJSONCommand extends AbstractDataCommand{

    @NotNull
    @Override
    public String name() {
        return "data-fasterxml-load-json";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Data FasterXML JSON load ...";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_JSON_FASTERXML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new JsonFactory());
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

}
